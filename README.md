# Customer Rewards App

The Reward Points App is a React application that calculates reward points for each transaction based on specific rules. It utilizes a simulated asynchronous API call to fetch transaction data.

## Getting Started

These instructions will guide you on downloading, installing, and running the project on your local machine.

### Prerequisites

Make sure you have Node.js and npm installed on your machine.

- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

### Installation

1. **Clone the Repository:**

   git clone https://gitlab.com/Nishanth_T/customer-rewards-app.git

2. **Navigate to the Project Directory:**
   
    cd customer-rewards-system

3. **Install Dependencies:**

    npm install

4. **Install JSON Server (Optional for Simulated API):**

    This is for simulate an API using JSON Server:
     npm install -g json-server

 ***Running the Application***

   1. **start json server**

      If you installed JSON Server and want to simulate an API, open a new terminal window and run:
      json-server --watch db.json

      db.json will store the transacton data. which will be stored under root directory.

   2. **start the React App**
        In the original terminal window, run:
        npm start

    Open your browser and go to http://localhost:3000 to view the app.
  

  3. **Running Tests**
     install jest-fetch-mock to mock the fetch requests
     npm install --save-dev jest jest-fetch-mock
     
     npm test






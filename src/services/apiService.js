
 const apiUrl = process.env.REACT_APP_API_URL;

const fetchTransactions = async () => {
  try {
    const response = await fetch(`${apiUrl}/transactions`);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error; 
  }
};

export default { fetchTransactions };

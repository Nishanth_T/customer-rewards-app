import React, { useState, useEffect } from 'react';
import helpers from '../utils/helpers';
import Transaction from './Transaction';


const TransactionList = ({ transactions }) => {
  const [monthlyRewards, setMonthlyRewards] = useState({});
  const [totalRewards, setTotalRewards] = useState(0);

  useEffect(() => {
    //rewards aggregation change
    if(!helpers.isEmpty(transactions)) {
      const monthlyRewardsMap = {};
      let totalRewards = 0;
  
      transactions.forEach((transaction) => {
        const { customerId, amount, date } = transaction;
        const reward = helpers.calculate(amount);
        totalRewards += reward;
  
        if (!monthlyRewardsMap[customerId]) {
          monthlyRewardsMap[customerId] = {};
        }
        const month = new Date(date).toLocaleString('en-US', { month: 'long' });
        monthlyRewardsMap[customerId][month] = {
          points: (monthlyRewardsMap[customerId][month]?.points || 0) + reward,
          amount: (monthlyRewardsMap[customerId][month]?.amount || 0) + amount
        };
      });
  
      setMonthlyRewards(monthlyRewardsMap);
      setTotalRewards(totalRewards);
    }
  }, [transactions]);

  return (
    <div>
      {!helpers.isEmpty(monthlyRewards) ? <div>
      <h2>Total Rewards: {totalRewards} points</h2>
      <h3>Monthly Rewards</h3>
      <div className='reward-container'>
        {Object.entries(monthlyRewards).map(([customerId, rewards]) => (
           <Transaction key={customerId} transactionId={customerId}  rewards={rewards}/>
        ))}
      </div>
      </div>: <div className='nodata-available'>No transactions available.</div>}
    </div>
  );
};

export default TransactionList;

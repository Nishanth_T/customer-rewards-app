import React from 'react';
import helpers from '../utils/helpers';

const Transaction = ({ transactionId, rewards }) => {
  return (
    <div className="transaction-card">
      <strong>Customer ID:</strong> {transactionId} <br />
       <ul>
      {!helpers.isEmpty(rewards) && Object.entries(rewards).map(([month, { points, amount}], index, array) => (
        <li key={`${transactionId}-${month}`}>
          <strong>Month:</strong> {month} <br />
          <strong>Points:</strong> {points} <br />
          <strong>Total Amount:</strong> {helpers.currency(amount)}<br />
          {index !== array.length - 1 && <hr />}
        </li>
      ))}
    </ul>
    </div>
  );
};

export default Transaction;

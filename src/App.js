import React, { useState, useEffect } from 'react';
import TransactionList from './components/TransactionList';
import apiService from './services/apiService';
import './styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Spinner from 'react-bootstrap/Spinner';

const App = () => {
  const [transactions, setTransactions] = useState([]);
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        setTimeout(async () => {
          const data = await apiService.fetchTransactions();
          setTransactions(data);
        }, 3000);
      } catch (error) {
        console.error('Error fetching data:', error);
      } finally {
        setTimeout(() => {
          setLoading(false);
        }, 3000);
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      {loading ? <div className="loader-container"><Spinner animation="border" role="status">
      </Spinner> </div> :
        <div>
          <h1>Customer Rewards Points Details</h1>
          <TransactionList transactions={transactions} /></div>}
    </div>
  );
};

export default App;


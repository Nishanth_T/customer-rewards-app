import * as constants from './constants';

const calculateRewardPoints = (amount) => {
  let rewardPoints = 0;

  if (amount > constants.POINTS_THRESHOLD_2) {
    rewardPoints += constants.POINTS_RATE_2 * (amount - constants.POINTS_THRESHOLD_2);
  }

  if (amount > constants.POINTS_THRESHOLD_1) {
    rewardPoints += constants.POINTS_RATE_1 * (Math.min(amount, constants.POINTS_THRESHOLD_2) - constants.POINTS_THRESHOLD_1);
  }

  return rewardPoints;
};

const currencyFormat = (amount) => {
  const currency = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(amount);
  return currency;
}

const isEmpty = (obj) => {
  return obj ? Object.keys(obj).length === 0 : true;
};

export default { calculate: calculateRewardPoints, currency: currencyFormat, isEmpty: isEmpty };

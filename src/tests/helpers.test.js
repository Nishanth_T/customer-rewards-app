import helpers from '../utils/helpers';
import * as constants from '../utils/constants';

describe('calculateRewardPoints', () => {
    test('should return 0 for an amount below POINTS_THRESHOLD_1', () => {
      expect(helpers.calculate(30)).toBe(0);
    });
  
    test('should calculate reward points based on POINTS_RATE_1 for amount between POINTS_THRESHOLD_1 and POINTS_THRESHOLD_2', () => {
      expect(helpers.calculate(75)).toBe(constants.POINTS_RATE_1 * (75 - constants.POINTS_THRESHOLD_1));
    });
  
    test('should calculate reward points based on POINTS_RATE_2 for amount above POINTS_THRESHOLD_2', () => {
      expect(helpers.calculate(120)).toBe(
        constants.POINTS_RATE_1 * (constants.POINTS_THRESHOLD_2 - constants.POINTS_THRESHOLD_1) +
        constants.POINTS_RATE_2 * (120 - constants.POINTS_THRESHOLD_2)
      );
    });
  });
  
  describe('currencyFormat', () => {
    test('should format the currency correctly for positive amount', () => {
      expect(helpers.currency(50)).toBe('$50.00');
    });
  
    test('should format the currency correctly for zero amount', () => {
      expect(helpers.currency(0)).toBe('$0.00');
    });

  });
  
  describe('isEmpty', () => {
    test('should return true for an empty object', () => {
      expect(helpers.isEmpty({})).toBe(true);
    });
  
    test('should return false for an object with properties', () => {
      expect(helpers.isEmpty({ name: 'John', age: 30 })).toBe(false);
    });
  });
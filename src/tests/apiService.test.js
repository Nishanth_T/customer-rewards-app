import apiService from '../services/apiService';
global.fetch = require('jest-fetch-mock');

describe('fetchTransactions', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  test('should fetch transactions successfully', async () => {
    const mockData = [
        { "customerId": 1, "amount": 120, "date": "2024-01-15" },
        { "customerId": 1, "amount": 60, "date": "2024-02-05" },
        { "customerId": 1, "amount": 110, "date": "2024-01-16" },
        { "customerId": 1, "amount": 60, "date": "2024-03-01" },
        { "customerId": 2, "amount": 80, "date": "2024-01-20" },
        { "customerId": 2, "amount": 130, "date": "2024-02-20" },
        { "customerId": 2, "amount": 130, "date": "2024-03-22" },
        { "customerId": 3, "amount": 110, "date": "2024-01-16" },
        { "customerId": 3, "amount": 110, "date": "2024-02-16" },
        { "customerId": 3, "amount": 110, "date": "2024-03-16" },
        { "customerId": 4, "amount": 110, "date": "2024-02-16" },
        { "customerId": 4, "amount": 110, "date": "2024-01-10" },
        { "customerId": 4, "amount": 110, "date": "2024-03-10" },
        { "customerId": 4, "amount": 110, "date": "2024-01-16" },
        { "customerId": 5, "amount": 110, "date": "2024-02-16" },
        { "customerId": 5, "amount": 110, "date": "2024-01-16" },
        { "customerId": 5, "amount": 110, "date": "2024-03-16" }   
      ]
    fetch.mockResponseOnce(JSON.stringify(mockData));

    const result = await apiService.fetchTransactions();

    expect(result).toEqual(mockData);
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(`${process.env.REACT_APP_API_URL}/transactions`);
  });
});
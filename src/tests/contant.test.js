import {
    POINTS_THRESHOLD_1,
    POINTS_THRESHOLD_2,
    POINTS_RATE_1,
    POINTS_RATE_2,
  } from '../utils/constants';
  
  describe('Points Constants', () => {
    test('POINTS_THRESHOLD_1 should be a number', () => {
      expect(typeof POINTS_THRESHOLD_1).toBe('number');
    });
  
    test('POINTS_THRESHOLD_2 should be a number', () => {
      expect(typeof POINTS_THRESHOLD_2).toBe('number');
    });
  
    test('POINTS_RATE_1 should be a number', () => {
      expect(typeof POINTS_RATE_1).toBe('number');
    });
  
    test('POINTS_RATE_2 should be a number', () => {
      expect(typeof POINTS_RATE_2).toBe('number');
    });
  
    test('POINTS_RATE_2 should be greater than POINTS_RATE_1', () => {
      expect(POINTS_RATE_2).toBeGreaterThan(POINTS_RATE_1);
    });
  });